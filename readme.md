# FAQ
Для запуска предоставлен базовый [docker-compose](ollama.yml) 


1. [ui-chat](ui-chat/ui-chat.md)
2. [Добавить модель с hugging face](add-model-hugging-face/add-model-hugging-face.md)
3. [Документы в качестве промпта](document-prompt/document-prompt.md)
4. [Документация по rest api](https://github.com/ollama/ollama/blob/main/docs/api.md)

P:S
Есть вероятность, что использование документов в качестве промпта с ui не приведет к супер результату, т.к.
модели очень чувствительны к используемым промптам и их нужно будет докручивать самостоятельно